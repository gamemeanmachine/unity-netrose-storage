# unity-remote-storage
This package contains a set of utilities to ease the use of external storage for NetRose.

# Install
To install this package you need to open the package manager in your project and:

  1. Add a scoped registry with:
     - "name": "AlephVault"
     - "url": "https://unity.packages.alephvault.com"
     - "scopes": ["com.alephvault"]
  2. And another registry with:
     - "name": "GameMeanMachine"
     - "url": "https://unity.packages.gamemeanmachine.com"
     - "scopes": ["com.gamemeanmachine"]
  3. Look for this package: `com.gamemeanmachine.unity.netrose.storage`.
  4. Install it.

# Usage

# Notes
This documentation has to be updated after the big migration.
